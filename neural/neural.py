import os
import sys
import json
import gevent
import logging

from gevent.core import callback

from volttron.platform import get_home, set_home
from volttron.platform.messaging import headers as headers_mod
from volttron.platform.vip.agent import Agent, PubSub, Core
from volttron.platform.agent import utils

from settings import remote_url, topics_prefixes_to_watch, heartbeat_period

# The agent starts with a method with the name of the agent
# which returns an object of the Agent's class.
def neural(config_path, **kwargs):

    # Load the configuration file
    config = utils.load_config(config_path)

    # Get the configuration variables from the file, with 
    # default values in case of a problem reading the
    # config file.
    ActuatorEnabled = config.get("ActuatorEnabled", False)
    WeatherEnabled = config.get("WeatherEnabled", True)
    WeatherPredicter = config.get("WeatherPredicter", "https://www.wunderground.com/"
    WeatherAgentOn = config.get("WeatherAgentOn", True)

    return NeuralAgent(ActuatorEnabled, WeatherEnabled, WeatherPredicter, WeatherAgentOn, **kwargs)

class NeuralAgent(Agent):
    def __init__(self, ActuatorEnabled=False, WeatherEnabled=True, WeatherPredicter="https://www.wunderground.com/", WeatherAgentOn=True, **kwargs):
        super(NeuralAgent, self).__init__(**kwargs)

        self.default_config = {"ActuatorEnabled": ActuatorEnabled,
                               "WeatherEnabled": WeatherEnabled,
                               "WeatherPredicter": WeatherPredicter,
                               "WeatherAgentOn": WeatherAgentOn}

        self.vip.config.set_default("config", self.default_config)

        self.wind = 0
        self.solar = 0
        self.temperature = 0
        self.humidity = 0

        # This line from the tutorial causes the configure_main
        # function to activate whenever the configuration file
        # is activated.  The configure_main function then checks
        # the new configuration file, and makes sure that the
        # inputs are correctly formed.
        self.vip.config.subscribe(self.configure_main, actions=["NEW", "UPDATE"], pattern="config")

        # When this starts up, I want to subscribe to the wind
        # speed, the solar radiation, the temperature, and the
        # humidity.  Each time it should call on the function
        # for storing weather data.  I'm a bit worried that I
        # might be using the topic name for the prefix, but
        # I haven't found anything else so far that looks like
        # it could be the topic path.
        self.vip.pubsub.subscribe(peer='pubsub', prefix="weather/wind/wind_gust_kph", callback=self.store_weather_data)
        self.vip.pubsub.subscribe(peer='pubsub', prefix="weather/could_cover/solarradiation", callback=self.store_weather_data)
        self.vip.pubsub.subscribe(peer='pubsub', prefix="weather/temperature/temp_c", callback=self.store_weather_data)
        self.vip.pubsub.subscribe(peer='pubsub', prefix="weather/pressure_humidity/relative_humidity", callback=self.store_weather_data)
        

    # This function should be called whenever someone starts
    # the agent with the "vctl start <vip>" command
    @Core.receiver('onstart')
    def start(self, sender, **kwargs):

        _log.debug("NeuralAgent started")

    # From the tutorial, I left this in just to tell me the agent is running.
    @Core.receiver(heartbeat_period)
    def publish_heartbeat(self):
        _log.debug("NeuralAgent beat")

    # This function should be called on whenever the weather is published.
    def store_weather_data(self, peer, sender, bus, topic, headers, message):
        if topic == "weather/wind/wind_gust_kph":
            self.wind = message
        elif topic == "weather/could_cover/solarradiation":
            self.solar = message
        elif topic == "weather/temperature/temp_c":
            self.temperature = message
        elif topic == "weather/pressure_humidity/relative_humidity":
            self.humidity = message

    # This function should be called periodically to activate the AI and
    # send it messages.  Right now it just prints to a json file.  At the
    # end it should send its results to the model.
    @Core.receiver(heartbeat_period)
    def send_to_neural(self):
        
        with open('datatransitfile1', 'w') as file:
            file.write("{")
            file.write("\"wind\":\"{}\"".format(self.wind))
            file.write("\"solar\":\"{}\"".format(self.solar))
            file.write("\"temperature\":\"{}\"".format(self.temperature))
            file.write("\"humidity\":\"{}\"".format(self.humidity))
            file.write("}")

    # You probably won't be able to write this until you have the model.
    # You might be able to invoke the model by importing it and invoking
    # it as a function.  If that's not possible, use the os.command()
    # function to activate it, and catch the feedback in the send_result
    # function.
    def get_from_neural(self):
        #with open('datatransitfile1', 'r') as file:
        pass
            

    # This uses rpc calls to call on an actuator.  You'll need to find
    # some way to predict what the actuator actually is in order to
    # call on it.  The order is:  
    # 1.  Schedule the actuator.
    # 2.  Send command to the actuator.
    # 3.  Unschedule the actuator.
    # These should only be done if the actuator is enabled in
    # the config file.  Otherwise, output the result for someone to see.
    def send_result(self, result):
        if config["ActuatorEnabled"]:
            self.vip.pubsub.publish('pubsub', topics.ACTUATOR_SCHEDULE_REQUEST, headers, msg) #Really need to change this line.
            #self.vip.rpc.call('platform.actuator', 'request_new_schedule', uid, task_id, 'LOW', 'Actuator Scheduled').get(timeout=5)
            #self.vip.rpc.call('platform.actuator', 'set_point', uid, task_id, device, value).get(timeout=5)
	    #self.vip.rpc.call('platform.actuator', 'request_cancel_schedule', uid, task_id).get(timeout=5)
        else:
            print result

    # This is a configuration method 
    def configure_main(self, config_name, action, contents):

        config = self.default_config.copy()
        config.update(contents)

        _log.debug("Configuring NeuralAgent")

        #This is to check that the config file is in the right format
        try:
            ActuatorEnabled = bool(config["ActuatorEnabled"])
            WeatherEnabled = bool(config["WeatherEnabled"])
            WeatherPredicter = str(config["WeatherPredicter"])
            WeatherAgentOn = bool(config["WeatherAgentOn"])
        except ValueError as e:
            _log.error("ERROR PROCESSING CONFIGURATION: {}".format(e))
            return

        _log.debug("Using ActuatorEnabled {}, WeatherEnabled {}, WeatherPredicter {}, WeatherAgentOn {}".format(ActuatoreEnabled, WeatherEnabled, WeatherPredicter, WeatherAgentOn))
